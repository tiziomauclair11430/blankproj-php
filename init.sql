DROP DATABASE IF EXISTS BlankprojPhp;
CREATE DATABASE BlankprojPhp;
USE BlankprojPhp;
GRANT ALL PRIVILEGES ON ti.* TO 'ti'@'localhost';

CREATE TABLE test1 (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom Varchar(50),
    crypt Varchar(50)
);

CREATE TABLE test2 (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom Varchar(50),
    crypt Varchar(50),
    nana Varchar(50)
);